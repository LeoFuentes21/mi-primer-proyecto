# catalogo/forms.py
from django import forms
from .models import Pais, Departamento, Municipio

class PaisForm(forms.ModelForm):
    class Meta:
        model = Pais
        fields = ['nombre']

#formulario para Departamentos 

class DepartamentoForm(forms.ModelForm):
    class Meta:
        model = Departamento
        fields = ['nombre']

#form para Municipios

class MunicipioForm(forms.ModelForm):
    class Meta:
        model = Municipio
        fields = ['nombre']
    