# catalogo/views.py
from django.shortcuts import render, get_object_or_404, redirect
from .models import Pais, Departamento, Municipio
from .forms import PaisForm, DepartamentoForm, MunicipioForm

def listar_paises(request):
    paises = Pais.objects.all()
    return render(request, 'listar_paises.html', {'paises': paises})

def agregar_pais(request):
    if request.method == 'POST':
        form = PaisForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('listar_paises')
    else:
        form = PaisForm()
    return render(request, 'catalogo/agregar_pais.html', {'form': form})

def modificar_pais(request, pais_id):
    pais = get_object_or_404(Pais, pk=pais_id)
    if request.method == 'POST':
        form = PaisForm(request.POST, instance=pais)
        if form.is_valid():
            form.save()
            return redirect('listar_paises')
    else:
        form = PaisForm(instance=pais)
    return render(request, 'catalogo/modificar_pais.html', {'form': form, 'pais': pais})

def eliminar_pais(request, pais_id):
    pais = get_object_or_404(Pais, pk=pais_id)
    if request.method == 'POST':
        pais.delete()
        return redirect('listar_paises')
    return render(request, 'catalogo/eliminar_pais.html', {'pais': pais})

#departamento

def listar_departamentos(request):
    departamentos = Departamento.objects.all()
    return render(request, 'listar_departamentos.html', {'departamentos': departamentos})

def agregar_departamento(request):
    if request.method == 'POST':
        form = DepartamentoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('listar_departamentos')
    else:
        form = DepartamentoForm()
    return render(request, 'catalogo/agregar_departamento.html', {'form': form})

def modificar_departamento(request, departamento_id):
    departamento = get_object_or_404(Departamento, pk=departamento_id)
    if request.method == 'POST':
        form = DepartamentoForm(request.POST, instance=departamento)
        if form.is_valid():
            form.save()
            return redirect('listar_departamentos')
    else:
        form = DepartamentoForm(instance=departamento)
    return render(request, 'catalogo/modificar_departamento.html', {'form': form, 'departamento': departamento})

def eliminar_departamento(request, departamento_id):
    departamento = get_object_or_404(Departamento, pk=departamento_id)
    if request.method == 'POST':
        departamento.delete()
        return redirect('listar_departamentos')
    return render(request, 'catalogo/eliminar_departamento.html', {'departamento': departamento})

#municipio

def listar_municipios(request):
    municipios = Municipio.objects.all()
    return render(request, 'listar_municipios.html', {'municipios': municipios})

def agregar_municipio(request):
    if request.method == 'POST':
        form = MunicipioForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('listar_municipios')
    else:
        form = MunicipioForm()
    return render(request, 'catalogo/agregar_municipio.html', {'form': form})

def modificar_municipio(request, municipio_id):
    municipio = get_object_or_404(Municipio, pk=municipio_id)
    if request.method == 'POST':
        form = MunicipioForm(request.POST, instance=municipio)
        if form.is_valid():
            form.save()
            return redirect('listar_municipios')
    else:
        form = MunicipioForm(instance=municipio)
    return render(request, 'catalogo/modificar_municipio.html', {'form': form, 'municipio': municipio})

def eliminar_municipio(request, municipio_id):
    municipio = get_object_or_404(Municipio, pk=municipio_id)
    if request.method == 'POST':
        municipio.delete()
        return redirect('listar_municipios')
    return render(request, 'catalogo/eliminar_municipio.html', {'municipio': municipio})