from django.urls import path
from . import views

urlpatterns = [
    path('listar_paises/', views.listar_paises, name='listar_paises'),
    path('agregar_pais/', views.agregar_pais, name='agregar_pais'),
    path('modificar_pais/<int:pais_id>/', views.modificar_pais, name='modificar_pais'),
    path('eliminar_pais/<int:pais_id>/', views.eliminar_pais, name='eliminar_pais'),

    path('listar_departamentos/', views.listar_departamentos, name='listar_departamentos'),
    path('agregar_departamento/', views.agregar_departamento, name='agregar_departamento'),
    path('modificar_departamento/<int:departamento_id>/', views.modificar_departamento, name='modificar_departamento'),
    path('eliminar_departamento/<int:departamento_id>/', views.eliminar_departamento, name='eliminar_departamento'),

    path('listar_municipios/', views.listar_municipios, name='listar_municipios'),
    path('agregar_municipio/', views.agregar_municipio, name='agregar_municipio'),
    path('modificar_municipio/<int:municipio_id>/', views.modificar_municipio, name='modificar_municipio'),
    path('eliminar_municipio/<int:municipio_id>/', views.eliminar_municipio, name='eliminar_municipio'),


   
]
